FROM maven:3.5.2-jdk-8

#ARG SonarRootCert="https://artifactory.jfrog.lfg.com/artifactory/edevops-release-package/certs/rootCA.cer"

#ARG CaCertPath="/etc/ca-certificates/rootCA.crt"


#ADD ${SonarRootCert} ${CaCertPath}


#RUN $JAVA_HOME/bin/keytool -import -alias sonar -noprompt -keystore $JAVA_HOME/jre/lib/security/cacerts -file ${CaCertPath} -storepass changeit \

#&& update-ca-certificates -f

#CMD ["mvn"]